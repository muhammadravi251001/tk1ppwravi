from django.db import models
from django.utils import timezone
from datetime import datetime, date

class DampakBerita(models.Model):
    nama_berita = models.CharField(max_length=30)
    nama_penulis = models.CharField(max_length=30)
    isi_berita = models.CharField(max_length=5000)

    def __str__(self):
        return self.nama_berita

#    phone_number = models.CharField(max_length=17)
#    id_line = models.CharField(max_length=6)

#class Post(models.Model):
#    author = models.ForeignKey(Person, on_delete =   
#      models.CASCADE)
#   content = models.CharField(max_length=125)
#    published_date =   
#      models.DateTimeField(default=timezone.now)


